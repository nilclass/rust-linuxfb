extern crate bindgen;

use std::env;
use std::path::PathBuf;

fn main() {
    println!("cargo:rerun-if-changed=bindings.h");

    let bindings = bindgen::Builder::default()
        .header("bindings.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .allowlist_type("fb_var_screeninfo")
        .allowlist_type("fb_fix_screeninfo")
        .allowlist_var("FBIOGET_VSCREENINFO")
        .allowlist_var("FBIOPUT_VSCREENINFO")
        .allowlist_var("FBIOGET_FSCREENINFO")
        .allowlist_var("FB_ACTIVATE_NOW")
        .allowlist_var("FBIOBLANK")
        .allowlist_var("FB_BLANK_.*")
        .allowlist_var("KDSETMODE")
        .allowlist_var("KD_TEXT")
        .allowlist_var("KD_GRAPHICS")
        .derive_default(true)
        .generate()
        .expect("Unable to generate bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());

    bindings
        .write_to_file(out_path.join("bindings.rs"))
        .expect("Couldn't write bindings");
}
